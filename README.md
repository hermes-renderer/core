
# So, what's this?

Hermes is a library that will take a markdown string representing source code
and generate a Vue component. It allows for custom components and nearly everything
about it is customizable.

![Image](https://i.imgur.com/wcIBEcI.gif "")

# Getting Started

You can start using Hermes by running:

```text
npm i -D @hermes-renderer/core
```

```text
yarn add -D @hermes-renderer/core
```

After that you can head to the `examples` folder and check out the
very long `README.md` there.

## Who's behind the project?

I'm behind it. I'm Riccardo, a mechatronics engineering student (not a professional developer, keep that in mind). I'm trying to set up a portfolio website using
Nuxt and Directus, and none of the other available editors seemed to be what I was looking for, so I tried building my own.
Very stressful, would not recommend.

I've worked on what you see here for about a week (today is the 25 September), so don't judge too harshly. I want to use the editor on my website,
so you can rest assured that as long as I have time to work on it, I will. I mean, i have a direct interest in the success of
this little project.

## What's the project structure?

The repository consists of the following branches:
- `main`: The main branch of the repository. (Hopefully) Stable code goes here.
- `experimental`: The experimental branch: this is where I (we?) will work most of the time.

# What is there to do?

_Everything_

I'll make a short list, off the top of my head:
- Tests
- Support for multiple entrypoints and exported modules
- A PDF renderer (one of these new entrypoints, should work server-side)
- A Library of common components (e.g. code blocks) that will be shipped by default with the renderer
- An editor
- A Directus plugin that allows editing and previewing on the dashboard

All of this will be worked on by me and you, if you're willing to help.
