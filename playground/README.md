#### **The TLDR is at the bottom!**

# Example

Welcome to the example page!
This is the same page that I use to develop and test the application.

It can be run by cloning the repository and running
`vite dev` in your terminal:
```
git clone git@gitlab.com:hermes-renderer/core.git hermes/core
cd hermes/core
yarn install
yarn dev
```

Now you can open the page in the prompt and play around with the shown
editor all you want.

You should type `localStorage.debug='hermes:*'` in your browser's console
to get a good look at what's going on behind the scenes.

## So, what is this?

This is a framework for developing your own renderer, which will accept
a markdown input and return a rendered Vue component for you to use on
your website. Many features work out of the box, but the renderer is
built to be heavily and easily customizable.

All you have to do to render your Markdown code is instantiate a
`<HermesRenderer>` Vue component and pass a couple props.

This component requires the props:
- `source`: the Markdown source code to be rendered
- `options`: the options to pass down to the renderer.

  They will be deep-merged with the default options.

The real power comes from the `options` object. It allows you to
customize _everything_ about how the abstract syntax tree is rendered.
Let's take a look at the inner workings of this component.

## The Inner Workings

**Don't skip this part! If you do you won't understand how to use this
thing well!**

As soon as the `<HermesRenderer>` component receives some Markdown source
code, it will generate an abstract syntax tree (AST).

For example, the Markdown text below:
```markdown
Hello! I'm a Paragraph.

[And I'm a Link!](test)

:::some-component{message="And I will be a component"}
:::
```
Will generate this abstract syntax tree:
```
{
    "type": "root",
    "children": [
        {
            "type": "paragraph",
            "children": [
                {
                    "type": "text",
                    "value": "Hello! I'm a Paragraph.",
                    ...
                }
            ],
            ...
        },
        {
            "type": "paragraph",
            "children": [
                {
                    "type": "link",
                    "title": null,
                    "url": "test",
                    "children": [
                        {
                            "type": "text",
                            "value": "And I'm a Link!",
                            ...
                        }
                    ],
                    ...
                }
            ],
            ...
        },
        {
            "type": "containerDirective",
            "name": "some-component",
            "attributes": {
                "message": "And I will be a component"
            },
            "children": [],
            ...
        }
    ],
    ...
}
```
The next step is to translate this into a Vue component. We can do this
through a collection of `Transformer` classes, which will transform nodes
into Vue components. Here is an example of a transformer:

```typescript
export default class Emphasis extends Transformer<Node, VNode, TransformerOptions> {

    constructor(debug: Debugger, options: TransformerOptions) {
        /*
         * The last argument of the super call will be the name
         * of this transformer. When the name corresponds to the node
         * type, this transformer will be called.
         */
        super(debug, options, 'emphasis');
    }

    public transform(node: Node, options: TransformerOptions, children: ChildrenData<Node, VNode>) {

        // Load object classes from configuration
        const classes: Array<string> | undefined = options?.theme?.text?.emphasis?.classes;

        return h('em', {
            class: classes?.length === 0 ? undefined : classes
        }, children.output);
    }

}
```

This `Transformer` will transform an abstract syntax tree node into a 
Vue component through the `h()` render function from Vue 3.

In this case, the result is a simple `<em>`
tag containing an arbitrary set of children.

An array of default transformers is already shipped with the renderer,
but you can overwrite them without getting a headache.
We will see that in a bit.

### Directives

The default transformers include transformers for something called
_Directives_, which are not part of default Markdown, and will allow you
to render your own components.

The format of these directives is specified [here](https://github.com/syntax-tree/mdast-util-directive#syntax-tree).

In case you didn't open the link above, there are 3 kinds of directives:
1) `textDirective`: Used for text decorators (e.g. setting a custom color) and such.
   The Directive:
   ```
   :name[Label]{#x.y.z key=value}
   ```
   Will become the node:
    ```
    {
      type: 'textDirective',
      name: 'name',
      attributes: {id: 'x', class: 'y z', key: 'value'},
      children: [{type: 'text', value: 'Label'}]
    }
    ```
2) `leafDirective`: Used for text decorators (e.g. setting a custom color) and such.
   The Directive:
   ```
   ::youtube[Label]{v=123}
   ```
   Will become the node:
    ```
    {
      type: 'leafDirective',
      name: 'youtube',
      attributes: {v: '123'},
      children: [{type: 'text', value: 'Label'}]
    }
    ```

3) `containerDirective`: Used for text decorators (e.g. setting a custom color) and such.
   The Directive:
   ```
   :::spoiler[Open at your own peril]
   He dies.
   :::
   ```
   Will become the node:
    ```
    {
        type: 'containerDirective',
            name: 'spoiler',
            attributes: {},
        children: [
            {
                type: 'paragraph',
                data: {directiveLabel: true},
                children: [{type: 'text', value: 'Open at your own peril'}]
            },
            {
                type: 'paragraph',
                children: [{type: 'text', value: 'He dies.'}]
            }
        ]
    }
    ```

This is where it gets confusing, so bear with me.

**Each one of these directives, in turn, has its own set of transformers!**

There is a set of transformers for `textDirective`, one for `leafDirective`
and one for `containerDirective`.

For example, this is the `default` transformer for a `textDirective`:

```typescript
export default class Default extends Transformer<Node, VNode, TransformerOptions> {

    constructor(debug: Debugger, options: TransformerOptions) {
        /*
         * The 'default' name is special.
         * This transformer will run only if no other transformers
         * have been defined for a certain container dicrective.
         */
        super(debug, options, 'default');
    }

    public transform(node: Node, options: TransformerOptions, children: ChildrenData<Node, VNode>) {
        
            const name = (node as ContainerDirective).name;

            // Try to find a component from the configuration
            let component: DefineComponent | undefined = undefined;
            if(options.components)
                component = options.components[name];

            // If a component with that name exists, return an instance
            if(component)
                return h(
                    component,
                    {
                        context: options.context
                    },
                    () => children.output
                );

        }

    }

}
```

The default `leafDirective` and `containerDirective` transformers
will look for components in the options and render those components.
So if you keep the default transformers you can just register a component,
type something like `::my-component{prop1=Hello, prop2=World}` and it will
work.

We will see why the fact that each directive has its own
`Transformer` matters to you in the Options section.

## Options

Probably the most important part of this wall of text.

```typescript
export default interface TransformerOptions {

    /**
     * Through this object you can specify custom transformers to take care of the rendering
     */
    transformers?: {
        markdown?: Array<TransformerConstructor_MD>,
        directive?: {
            text?: Array<TransformerConstructor_DT>,
            leaf?: Array<TransformerConstructor_DL>,
            container?: Array<TransformerConstructor_DC>,
        }
    },

    /**
     * The context object is used in library classes to show dynamic content through interpolation.
     */
    context: Record<string, unknown>,

    text: {
        interpolate: boolean
    },

    /**
     * This is the registry of components to which the renderer will have access.
     * If you want a component to be rendered from your Markdown code, it should be here.
     */
    components?: Record<string, DefineComponent>,

    /**
     * This is the renderer's theme. Through this, you can set classes on the default elements in the
     * renderer tree, such as spans and such. It also contains other cosmetic settings, but mainly uses
     * classes in order to guarantee flexibility across frameworks.
     */
    theme: Theme
}
```

Let's go through each one of these configuration options:

- `transformers`: A _transformer_ is a class that allows you to turn
  something into something else. (A bit generic, I know).
  I wrote about them earlier.

  - Transformers stored in the `markdown` key are used to transform
  properties of the abstract syntax tree into Vue components.
  Nodes of type `link`, `list`, `heading` etc. will pass through these
  transformers. Have a look at the transformers 
  in the folder `src/vue/transformers/markdown`.

  - Transformers stored in the `directive.text` key are used to transform
  a `textDirective` into something else. Have a look at the transformers
  in the folder `src/vue/transformers/directive/text`.

  - Transformers stored in the `directive.leaf` key are used to transform
    a `leafDirective` into something else. Have a look at the transformers
    in the folder `src/vue/transformers/directive/leaf`.

  - Transformers stored in the `directive.container` key are used to transform
    a `containerDirective` into something else. Have a look at the transformers
    in the folder `src/vue/transformers/directive/container`.


- `context`: This context object will be passed down to every component
  and transformer. Notably, it's used for text interpolation:
  if, for example, `context.title = 'Hello World'`, `# {{ title }}` will be
  rendered into `# Hello World`


- `text`: Text options. You can enable or disable text interpolation here.


- `components`: If you want a component to be accessible to the renderer,
  you must add an entry here.
  For example, if you want to be able to render
  `::my-component{prop1=Hello, prop2=World}`
  you must define the entry `components['my-component'] = SomeComponent`.


- `theme`: This is the theme that the default renderer will use.
  To see what the possible options are, the easiest thing is to check
  out `src/vue/options/Theme` and the default
  themes in `src/vue/options/default/Theme`

# TLDR;

### I want to render a custom component!

Pass this option object to `<HermesRenderer>`:
```typescript
import MyComponent from './MyComponent.vue';
const options = {
    components: {
        'my-component': MyComponent
    }
}
```

Now when you write `::my-component{props1=Hello, prop2=World}` or
```
:::my-component{props1=Hello, prop2=World}
This will go in the default slot!
:::
```
your component will be rendered.

Any content will be placed in the `default` slot.

### I want to render the content of some variables!

Pass this option object to `<HermesRenderer>`:
```typescript
import MyComponent from './MyComponent.vue';
const options = {
    context: {
        myVariable: 'Hello World!'
    }
}
```

Now you will be able to use interpolation as follows:
```markdown
# {{ myVariable }}

This was defined in the context: {{ myVariable }}
```

### I want to render `<li>` elements in some super weird way!

Sure, you can do that.

Create a custom transformer somewhere in your project:
```typescript
export default class MyWeirdTransformer extends Transformer<Node, VNode, TransformerOptions> {

    constructor(debug: Debugger, options: TransformerOptions) {
        /*
         * 'listItem' is the type of the asbtract syntax tree that
         *  we obtain from the markdown file.
         *  More about this before the TLDR section.
         */
        super(debug, options, 'listItem');
    }

    public transform(node: Node, options: TransformerOptions, children: ChildrenData<Node, VNode>) {
        const item = node as MDListItem;
        // Load object classes from configuration
        const classes: Array<string> | undefined = options?.theme?.list?.li?.classes;
        /*
         * For the sake of playground, say that you want to render all 
         * <li> elements in your tree as '<li>My Dad is an old Fart</li>'
         */
        return h('li', {
        }, 'My Dad is an old Fart');
    }
}
```

Now import this transformer in the file where you instantiate
`<HermesRenderer>` and add it to the options like so:

```typescript
import MyTransformer from './somewhere/MyTransformer';
const options = {
    transformers: {
        markdown: [ MyTransformer ]
    }
}
```

Notice that we inserted the transformer in the `markdown` key
because its name `listItem` is the name of a node of the abstract syntax
tree and not the name of a directive.

### I want to render a text / leaf / content directive in some super weird way!

Remember that the rendering of custom components is already included
in the default functionalities of this library.

There are however some use cases where you might want to
handle the rendering directly. For example, if you wanted to build
a transformer for the `content` leaf directive you could use this
transformer:

```typescript
export default class MyMathTransformer extends Transformer<Node, VNode, TransformerOptions>  {

    constructor(debug: Debugger, options: TransformerOptions) {
        super(debug, options, 'math');
    }

    public transform(node: Node, options: TransformerOptions, children: ChildrenData<Node, VNode>) {

        const directive = (node as ContainerDirective);

        // We need to concatenate all the children text
        let source = '';

        for(const child of directive.children){
            if((child as Node).type === 'text')
                source += (child as Literal).value;
            if(child.type === 'paragraph')
                for(const subchild of child.children)
                    if(subchild.type === 'text')
                        source+= subchild.value;
        }

        // Add a tag if it was specified
        if(directive.attributes && directive.attributes.tag)
            source = `\\tag{${directive.attributes.tag}} ` + source;

        this.debug('Created Math String: %O', source);

        const html = katex.renderToString(source, {
            displayMode: true
        });

        // Load object classes from configuration
        const classes: Array<string> | undefined = options?.theme?.math?.block?.classes;

        return h('div', {
            class: classes?.length === 0 ? undefined : classes,
            innerHTML: html
        });

    }

}
```

You would then have to pass this transformer to the renderer options:

```typescript
import MyTransformer from './somewhere/MyMathTransformer';
const options = {
    transformers: {
        container: [ MyTransformer ]
    }
}
```

Notice that we inserted the transformer in the `container` key
because we want it to be used when we encounter
a **container** directive with name `math`, which would be something like this:

```
:::math{tag=1}
L = \frac{1}{2} \rho v^2 S C_L
:::
```
