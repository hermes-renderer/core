import Test from './Example.vue';
import {VuePlugin} from '@hermes/vue';
import {createApp} from 'vue';

async function client() {
    const app = await createApp(Test);
    app.use(VuePlugin);
    app.mount('#app');
}

client();

