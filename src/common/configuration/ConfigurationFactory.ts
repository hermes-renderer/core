import {Debugger} from 'debug';


export default class ConfigurationFactory<ConfigurationObject extends Record<string, unknown>> {

    protected debug: Debugger;
    protected configuration: ConfigurationObject | undefined;

    constructor(debug: Debugger, configuration?: ConfigurationObject) {
        this.debug = debug.extend('configuration');

        if (configuration)
            this.configuration = ConfigurationFactory.clone(configuration);

    }

    public load(configuration: ConfigurationObject) {
        if (this.configuration)
            // Merge the new configuration
            ConfigurationFactory.merge(this.configuration, configuration);
        else
            // Clone the configuration
            this.configuration = ConfigurationFactory.clone(configuration);
    }

    public get<Type>(identifier: string): Type | undefined {

        if(!this.configuration)
            return undefined;

        if (!/([a-z]+)(\.[a-z]+)*/gi.test(identifier))
            throw new Error(`The identifier ${identifier} is invalid.`);

        let bits = identifier.split('.');
        bits = bits.filter((bit) => bit.trim());

        let object: unknown | undefined;
        for (const bit of bits) {
            if (!object)
                object = this.configuration;
            const current = object as Record<string, unknown>;
            if (current[bit])
                object = current[bit];
        }
        return object as (Type | undefined);
    }

    protected set<Type>(identifier: string, value: Type): void {

        if(!this.configuration)
            return;

        if (!/([a-z]+)(\.[a-z]+)*/gi.test(identifier))
            throw new Error(`The identifier ${identifier} is invalid.`);

        let bits = identifier.split('.');
        bits = bits.filter((bit) => bit.trim());

        let object: unknown | undefined;

        for (let i = 0; i < bits.length - 1; i++) {
            if (!object)
                object = this.configuration as ConfigurationObject;
            const current = object as Record<string, unknown>;
            if (current[bits[i]])
                object = current[bits[i]];
        }

        (object as Record<string, unknown>)[bits[bits.length - 1]] = value;

    }

    protected static isObject(item: unknown) {
        return (item && typeof item === 'object' && !Array.isArray(item));
    }

    /**
     * This function will clone any object.
     * Particularly useful for configuration objects.
     * @param item The item to be cloned.
     */
    protected static clone<Type>(item: Type): Type {

        if (ConfigurationFactory.isObject(item)) {
            const result: Record<string, unknown> = {};
            for (const [key, value] of Object.entries(item as Record<string, unknown>)) {
                result[key] = ConfigurationFactory.clone(value) as unknown;
            }
            return result as Type;

        } else {

            // If the object is an Array, clone it
            if (Array.isArray(item))
                return [...item] as Type;

            return item as Type;

        }

    }

    /**
     * This function will deep-merge two different objects.
     * Particularly useful for configurations.
     * @param target The target of the merge operation.
     * @param sources Sources of data for the merge process.
     */
    protected static merge(target: Record<string, unknown>, ...sources: Record<string, unknown>[]) {

        if (!sources.length)
            return target;

        const source = sources.shift();

        if (ConfigurationFactory.isObject(target) && ConfigurationFactory.isObject(source)) {
            for (const key in source) {
                if (ConfigurationFactory.isObject(source[key])) {
                    if (!target[key]) Object.assign(target, {[key]: {}});
                    ConfigurationFactory.merge(target[key] as Record<string, unknown>, source[key] as Record<string, unknown>);
                } else {

                    // Clone arrays
                    if (Array.isArray(source[key])) {
                        Object.assign(target, {[key]: [...(source[key] as Array<unknown>)]});
                        return;
                    }

                    Object.assign(target, {[key]: source[key]});
                }

            }
        }

    }


}

