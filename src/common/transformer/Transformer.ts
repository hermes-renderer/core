import {Debugger} from 'debug';
import ConfigurationFactory from '@hermes/common/configuration/ConfigurationFactory';

export default abstract class Transformer<Input, Output, ConfigurationObject extends Record<string, unknown>> {

    protected name: string;
    protected debug: Debugger;
    protected configuration: ConfigurationFactory<ConfigurationObject>;

    /**
     * @param debug The debug instance that the transformer will use
     * @param configuration The configuration for the transformer
     * @param args Optional arguments. The first argument is used as the transformer name.
     */
    constructor(debug: Debugger, configuration: ConfigurationFactory<ConfigurationObject>, ...args: unknown[]) {
        this.name = (args ? args[0] : 'UNDEFINED') as string;
        this.debug = debug.extend(this.name);
        this.configuration = configuration;
    }

    public getName() { return this.name; }

    /**
     * This is the main transformer function that all transformers will have.
     * It will transform an Input type into an Output type.
     *
     * For playground, this is used to transform a Node (which represents text, for playground) into
     * a VNode (which is a Vue component).
     *
     * @param input The input object
     * @param args Optional arguments to be passed to the transformer
     */
    public abstract transform(input: Input, ...args: unknown[]): Output | undefined

}
