import {Debugger} from 'debug';
import Transformer from '@hermes/common/transformer/Transformer';
import ConfigurationFactory from '@hermes/common/configuration/ConfigurationFactory';

export default class TransformerContainer<Input, Output, ConfigurationObject extends Record<string, unknown>> {

    protected name: string;
    protected debug: Debugger;
    protected transformers: Array<Transformer<Input, Output, ConfigurationObject>> = [];
    protected configuration: ConfigurationFactory<ConfigurationObject>;

    constructor(debug: Debugger, configuration: ConfigurationFactory<ConfigurationObject>, ...args: unknown[]) {
        this.name = (args ? args[0] : 'UNDEFINED') as string;
        this.debug = debug.extend(this.name);
        this.configuration = configuration;
    }

    public getName() {
        return this.name;
    }

    /* ######## Transformer ######## */

    public addTransformer(transformer_class: new(debug: Debugger, options: ConfigurationFactory<ConfigurationObject>, ...args: unknown[]) => Transformer<Input, Output, ConfigurationObject>, name?: string): void {
        // Instantiate Transformer Class
        const transformer = new transformer_class(this.debug, this.configuration, name);
        // Check if a library for the given type already exists
        const results = this.transformers.filter((tr) => {
            return transformer.getName() !== tr.getName();
        });

        if (results.length < this.transformers.length) {
            this.debug('Replacing Transformer: %o', transformer.getName());
        }

        this.transformers = results;
        this.transformers.push(transformer);

    }

    public removeTransformer(type: string): void {
        // Check if a library for the given type already exist
        this.transformers = this.transformers.filter((tr) => {
            return tr.getName() !== type;
        });
    }

    public getTransformer(type: string): Transformer<Input, Output, ConfigurationObject> | undefined {
        const results = this.transformers.filter((tr) => {
            return tr.getName() === type;
        });
        return results.length > 0 ? results[0] : undefined;
    }

    public getTransformers() {
        return this.transformers;
    }

}
