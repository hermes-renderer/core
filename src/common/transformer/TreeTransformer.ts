import TransformerContainer from '@hermes/common/transformer/TransformerContainer';
import {Node, Parent} from 'unist';

export interface ChildrenData<Input, Output> {
    input?: Array<Input>,
    output?: Array<Output>
}

/**
 * This class is made to handle a unist data tree. It will transform the data tree to whatever
 * Output is.
 */
export default class TreeTransformer<Output, ConfigurationObject extends Record<string, unknown>> extends TransformerContainer<Node, Output, ConfigurationObject> {

    /**
     * This method will be used to transform an entire tree.
     *
     * @param node the root node of the tree
     */
    public transform(node: Node): Output | undefined {

        const parent = node as Parent;
        const children: ChildrenData<Node, Output> = {};
        if (parent.children)
            for (const child of parent.children) {

                const childComponent = this.transform(child);
                if (!childComponent)
                    continue;

                if(!children.input)
                    children.input = [child];
                else
                    children.input.push(child);

                if(!children.output)
                    children.output = [childComponent];
                else
                    children.output.push(childComponent);

            }

        for (const tr of this.transformers)
            if (tr.getName() === node.type) {
                return tr.transform(node, children);
            }

        this.debug('Could not find transformer for node of type %o', node.type);
        this.debug('Node Object: %O', node);

    }

}
