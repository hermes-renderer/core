import {Component, DefineComponent} from 'vue';
import VueTheme from '@hermes/vue/configuration/VueTheme';
import {
    ContainerTransformerConstructor,
    LeafTransformerConstructor,
    MarkdownTransformerConstructor,
    TextTransformerConstructor
} from '@hermes/vue/transformers/types';

export default interface VueConfiguration extends Record<string, unknown> {

    /**
     * Through this object you can specify custom transformers to take care of the rendering
     */
    transformers?: {
        markdown?: Array<MarkdownTransformerConstructor>,
        directive?: {
            text?: Array<TextTransformerConstructor>,
            leaf?: Array<LeafTransformerConstructor>,
            container?: Array<ContainerTransformerConstructor>,
        }
    },

    /**
     * The context object is used in library classes to show dynamic content through interpolation.
     */
    context?: Record<string, unknown>,

    text?: {
        interpolate?: boolean
    },

    /**
     * This is the registry of components to which the renderer will have access.
     * If you want a component to be rendered from your Markdown code, it should be here.
     */
    components?: Record<string, Component>,

    /**
     * This is the renderer's theme. Through this, you can set classes on the default elements in the
     * renderer tree, such as spans and such. It also contains other cosmetic settings, but mainly uses
     * classes in order to guarantee flexibility across frameworks.
     */
    theme?: VueTheme
}
