import ConfigurationFactory from '@hermes/common/configuration/ConfigurationFactory';
import VueConfiguration from '@hermes/vue/configuration/VueConfiguration';
import {
    ContainerTransformerConstructor,
    LeafTransformerConstructor,
    MarkdownTransformerConstructor,
    TextTransformerConstructor
} from '@hermes/vue/transformers/types';


export class VueConfigurationFactory extends ConfigurationFactory<VueConfiguration> {

    public load(configuration: VueConfiguration) {

        this.debug('Loading Configuration: %O', configuration);

        /*
         * Transformers get a special treatment.
         * When a configuration is merged, they will be appended rather than overwritten.
         */

        const old_transformers = ConfigurationFactory.clone(this.configuration?.transformers);

        // Merge the new configuration
        if (this.configuration)
            ConfigurationFactory.merge(this.configuration, configuration);

        const new_transformers = configuration.transformers;

        this.debug('Configuration after Merge: %O', this.configuration);

        type TransformerConstructor =
            TextTransformerConstructor
            | LeafTransformerConstructor
            | ContainerTransformerConstructor
            | MarkdownTransformerConstructor;

        const getTransformers = (
            old_transformers: Array<TransformerConstructor> | undefined,
            new_transformers: Array<TransformerConstructor> | undefined
        ) => {
            if (old_transformers && new_transformers)
                return [...old_transformers, ...new_transformers];
            else if (old_transformers)
                return [...old_transformers];
            else if (new_transformers)
                return [...new_transformers];
        };

        const markdown = getTransformers(old_transformers?.markdown, new_transformers?.markdown);
        const text = getTransformers(old_transformers?.directive?.text, new_transformers?.directive?.text);
        const leaf = getTransformers(old_transformers?.directive?.leaf, new_transformers?.directive?.leaf);
        const container = getTransformers(old_transformers?.directive?.container, new_transformers?.directive?.container);

        this.set('transformers.markdown', markdown);
        this.set('transformers.directive.text', text);
        this.set('transformers.directive.leaf', leaf);
        this.set('transformers.directive.container', container);

        this.debug('Configuration After Transformer Injection: %O', this.configuration);

    }

    public get<Type>(identifier: string) {
        let value = super.get(identifier);

        if (Array.isArray(value) && value.length === 0)
            value = undefined;

        return ConfigurationFactory.clone(value as (Type | undefined));
    }

}
