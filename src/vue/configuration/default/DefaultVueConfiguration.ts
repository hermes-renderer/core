import VueConfiguration from '@hermes/vue/configuration/VueConfiguration';
import tailwindDark from '../theme/TailwindDark';

// Transformers - Markdown
import * as Markdown from '@hermes/vue/transformers/markdown';
// Transformers - Directive
import * as Text from '@hermes/vue/transformers/directive/text';
import * as Leaf from '@hermes/vue/transformers/directive/leaf';
import * as Container from '@hermes/vue/transformers/directive/container';

const configuration: VueConfiguration = {

    context: {},

    text: {
        interpolate: true
    },

    theme: tailwindDark,

    transformers: {
        directive: {
            text: [
                Text.Default
            ],
            leaf: [
                Leaf.Default
            ],
            container: [
                Container.Default,
                Container.Math
            ]
        },
        markdown: [
            Markdown.Root,

            Markdown.Text,
            Markdown.Paragraph,
            Markdown.Heading,
            Markdown.Link,

            Markdown.Emphasis,
            Markdown.Strong,

            Markdown.InlineCode,
            Markdown.Code,

            Markdown.InlineMath,
            Markdown.Math,

            Markdown.List,
            Markdown.ListItem,

            Markdown.TextDirective,
            Markdown.LeafDirective,
            Markdown.ContainerDirective
        ]
    }

};

export default configuration;
