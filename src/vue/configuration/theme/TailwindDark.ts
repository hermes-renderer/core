import VueTheme from '@hermes/vue/configuration/VueTheme';

const theme: VueTheme = {

    container: {
        classes: [
            'bg-slate-800',
            'text-white',
            'p-4',
            'rounded',
            'leading-loose'
        ]
    },

    text: {
        colors: {
            'red': 'text-red-500'
        }
    },

    heading: {
        classes: {
            default: [
                'font-bold',
                'mb-4'
            ],
            depth: {
                '1': 'text-5xl',
                '2': 'text-4xl',
                '3': 'text-3xl',
                '4': 'text-2xl',
                '5': 'text-1xl',
                '6': 'text-lg'
            }
        }
    },

    paragraph: {
        classes: [
        ]
    },

    list: {
        li: {
            classes: []
        },
        ul: {
            classes: [
                'list-disc list-inside'
            ]
        },
        ol: {
            classes: [
                'list-decimal list-inside'
            ]
        }
    },

    link: {
        classes: [
            'text-red-500',
            'hover:underline'
        ]
    },

    code: {
        inline: {
            classes: [
                'p-1',
                'bg-teal-900',
                'rounded'
            ]
        },
        block: {
            classes: [
                'leading-snug',
                'p-2',
                'bg-teal-900',
                'rounded'
            ]
        }
    }

};

export default theme;
