import {App} from 'vue';
import HermesRenderer from './components/HermesRenderer.vue';

export const VuePlugin = {
    install(app: App) {
        app.component('HermesRenderer', HermesRenderer);
    }
};

// Export the processor
export {default as MarkdownProcessor} from './processors/MarkdownProcessor';

// Export Configuration
import {default as VueConfiguration} from './configuration/VueConfiguration';
export type Configuration = VueConfiguration;
