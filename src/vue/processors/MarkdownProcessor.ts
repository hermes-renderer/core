import VueConfiguration from '@hermes/vue/configuration/VueConfiguration';
import VuePlugin from './unified/VuePlugin';
import remarkDirective from 'remark-directive';
import remarkFrontmatter from 'remark-frontmatter';
import remarkMath from 'remark-math';
import remarkParse from 'remark-parse';
import yaml from 'yaml';
import {Debugger} from 'debug';
import {VNode} from 'vue';
import {Literal, Node, Parent} from 'unist';
import {VFile} from 'vfile';
import {unified} from 'unified';
import DefaultOptions from '@hermes/vue/configuration/default/DefaultVueConfiguration';
import {VueConfigurationFactory} from '@hermes/vue/configuration/VueConfigurationFactory';

/**
 * This class will handle the loading and processing of raw markdown data.
 */
export default class MarkdownProcessor {

    protected debug: Debugger;
    private tree: Parent | undefined;

    constructor(debug: Debugger) {
        this.debug = debug.extend('markdown');
    }

    public getTree(){ return this.tree; }

    /**
     * This method will load a MDAST tree from the passed data.
     * This tree represents the content of the Markdown file.
     * @param data content of the markdown file
     */
    public async load(data: string){
        this.tree = await unified()
            .use(remarkDirective)
            .use(remarkParse)
            .use(remarkFrontmatter)
            .use(remarkMath)
            .parse(data);
        this.debug('Loaded Data Tree: %O', this.tree);
    }


    /**
     * This method will return the frontmatter of the currently loaded Markdown document.
     * Currently, it only supports yaml, in order not to bloat the final module too much.
     */
    public front(): unknown {

        if(!this.tree)
            return {};

        try {
            // Find the first root-level YAML child
            for(const child of this.tree.children){
                if(child.type === 'yaml'){
                    const yaml_text = (child as Literal).value as string;
                    const front = yaml.parse(yaml_text);
                    this.debug('Detected Front Matter (YAML): %O', front);
                    return front;
                }
            }
        } catch(err) {
            this.debug('Error parsing front YAML: %O', err);
        }

        return {};

    }

    /**
     * This method will render a Vue component from the loaded MDAST tree.
     * @param options configuration for the rendering process
     */
    public async vue(options: VueConfiguration): Promise<VNode> {

        const configurationFactory = new VueConfigurationFactory(this.debug, DefaultOptions);
        configurationFactory.load(options);

        const file: VFile = new VFile();
        await unified()
            .use(VuePlugin, {
                debug: this.debug,
                configuration: configurationFactory
            })
            .run(this.tree as Node, file);

        return file.result as VNode;
    }

    /**
     * This method will render a pdf file from the loaded MDAST tree.
     * @param configuration configuration for the rendering process
     */


}
