import TreeTransformer from '@hermes/common/transformer/TreeTransformer';
import {Debugger} from 'debug';
import {DefineComponent, VNode} from 'vue';
import {Node} from 'unist';
import {Plugin} from 'unified';
import {VFile} from 'vfile';
import {MarkdownTransformerConstructor} from '@hermes/vue/transformers/types';
import {VueConfigurationFactory} from '@hermes/vue/configuration/VueConfigurationFactory';
import VueConfiguration from '@hermes/vue/configuration/VueConfiguration';

interface VuePluginOptions {
    configuration: VueConfigurationFactory,
    debug: Debugger
}

export default function (options: VuePluginOptions): Plugin<any, Node, DefineComponent> {

    return (tree: Node, file: VFile) => {

        /* ######## Component Rendering ######## */
        const renderer = new TreeTransformer<VNode, VueConfiguration>(options.debug, options.configuration, 'vue');

        // Markdown Transformers - Options
        const transformers = options.configuration.get<Array<MarkdownTransformerConstructor>>('transformers.markdown');
        if (transformers)
            for (const tf of transformers)
                renderer.addTransformer(tf);

        file.result = renderer.transform(tree);

    };

}
