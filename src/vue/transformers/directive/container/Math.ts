import {Literal, Node} from 'unist';
import {h, VNode} from 'vue';
import Transformer from '@hermes/common/transformer/Transformer';
import {ChildrenData} from '@hermes/common/transformer/TreeTransformer';
import {Debugger} from 'debug';
import {ContainerDirective} from 'mdast-util-directive';
import HermesError from '@hermes/vue/components/library/HermesError.vue';
import katex from 'katex';
import {VueConfigurationFactory} from '@hermes/vue/configuration/VueConfigurationFactory';
import VueConfiguration from '@hermes/vue/configuration/VueConfiguration';

export default class Math extends Transformer<Node, VNode, VueConfiguration>  {

    constructor(debug: Debugger, configuration: VueConfigurationFactory) {
        super(debug, configuration, 'math');
    }

    public transform(node: Node, children: ChildrenData<Node, VNode>) {

        const directive = (node as ContainerDirective);

        // We need to concatenate all the children text
        let source = '';

        for(const child of directive.children){
            if((child as Node).type === 'text')
                source += (child as Literal).value;
            if(child.type === 'paragraph')
                for(const subchild of child.children)
                    if(subchild.type === 'text')
                        source+= subchild.value;
        }

        // Add a tag if it was specified
        if(directive.attributes && directive.attributes.tag)
            source = `\\tag{${directive.attributes.tag}} ` + source;

        // Load object classes from configuration
        const classes = this.configuration.get<string[]>('theme.math.block.classes');

        let html: string | undefined;
        let error: unknown | undefined;

        try {
            html = katex.renderToString(source, {
                displayMode: true
            });
        } catch(err){
            error = err;
        }

        if(error){
            return h(HermesError, {
                class: classes,
                error: error
            });
        } else {
            return h('div', {
                class: classes,
                innerHTML: html
            });
        }

    }

}
