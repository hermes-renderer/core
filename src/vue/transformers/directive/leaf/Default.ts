import {Node} from 'unist';
import {DefineComponent, h, VNode} from 'vue';
import Transformer from '@hermes/common/transformer/Transformer';
import {ChildrenData} from '@hermes/common/transformer/TreeTransformer';
import {Debugger} from 'debug';
import {ContainerDirective} from 'mdast-util-directive';
import {VueConfigurationFactory} from '@hermes/vue/configuration/VueConfigurationFactory';
import VueConfiguration from '@hermes/vue/configuration/VueConfiguration';

export default class Default extends Transformer<Node, VNode, VueConfiguration>  {

    constructor(debug: Debugger, configuration: VueConfigurationFactory) {
        super(debug, configuration, 'default');
    }

    public transform(node: Node, children: ChildrenData<Node, VNode>) {

        const name = (node as ContainerDirective).name;

        // Try to find the component from configuration
        let component: DefineComponent | undefined = undefined;

        const components = this.configuration.get<Record<string, DefineComponent>>('components');
        if(components)
            component = components[name];

        if(component)
            return h(
                component,
                {
                    context: this.configuration.get('context')
                },
                () => children.output
            );

    }

}
