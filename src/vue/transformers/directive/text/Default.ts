import {Node} from 'unist';
import {h, VNode} from 'vue';
import Transformer from '@hermes/common/transformer/Transformer';
import {ChildrenData} from '@hermes/common/transformer/TreeTransformer';
import {Debugger} from 'debug';
import {TextDirective} from 'mdast-util-directive';
import {VueConfigurationFactory} from '@hermes/vue/configuration/VueConfigurationFactory';
import VueConfiguration from '@hermes/vue/configuration/VueConfiguration';

export default class Default extends Transformer<Node, VNode, VueConfiguration> {

    constructor(debug: Debugger, configuration: VueConfigurationFactory) {
        super(debug, configuration, 'default');
    }

    public transform(node: Node, children: ChildrenData<Node, VNode>) {

        const directive = (node as TextDirective);

        // Check if the directive name is a color
        const colors = this.configuration.get<Record<string, string>>('theme.text.colors');
        const color = colors ? colors[directive.name] : undefined;

        if (color) {
            return h(
                'span',
                {
                    id: directive.attributes?.id,
                    class: [directive.attributes?.class, color]
                },
                children.output
            );
        }

        // If all else fails, return a simple span
        return h(
            'span',
            {
                ...directive.attributes
            },
            children.output
        );

    }

}
