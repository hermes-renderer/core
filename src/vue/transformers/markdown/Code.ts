import {h, VNode} from 'vue';
import {Code as MDCode} from 'mdast';
import {Node} from 'unist';
import Transformer from '@hermes/common/transformer/Transformer';
import {Debugger} from 'debug';
import {ChildrenData} from '@hermes/common/transformer/TreeTransformer';
import {VueConfigurationFactory} from '@hermes/vue/configuration/VueConfigurationFactory';
import VueConfiguration from '@hermes/vue/configuration/VueConfiguration';

export default class Code extends Transformer<Node, VNode, VueConfiguration> {

    constructor(debug: Debugger, options: VueConfigurationFactory) {
        super(debug, options, 'code');
    }

    public transform(node: Node, children: ChildrenData<Node, VNode>) {

        // Load object classes from configuration
        const classes = this.configuration.get<string[]>('theme.code.block.classes');

        const code = node as MDCode;
        return h('pre', {
            class: classes,
            data: code.value,
            language: code.lang
        }, code.value);
    }

}
