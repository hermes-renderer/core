import {Node} from 'unist';
import {VNode} from 'vue';
import Transformer from '@hermes/common/transformer/Transformer';
import {ChildrenData} from '@hermes/common/transformer/TreeTransformer';
import {Debugger} from 'debug';
import TransformerContainer from '@hermes/common/transformer/TransformerContainer';
import {ContainerDirective as MDContainerDirective} from 'mdast-util-directive';
import {VueConfigurationFactory} from '@hermes/vue/configuration/VueConfigurationFactory';
import {ContainerTransformerConstructor} from '@hermes/vue/transformers/types';
import VueConfiguration from '@hermes/vue/configuration/VueConfiguration';

export default class ContainerDirective extends Transformer<Node, VNode, VueConfiguration>  {

    protected container: TransformerContainer<Node, VNode, VueConfiguration>;

    constructor(debug: Debugger, configuration: VueConfigurationFactory) {
        super(debug, configuration, 'containerDirective');
        this.container = new TransformerContainer<Node, VNode, VueConfiguration>(debug, configuration, 'container-directive');

        // Load all the transformers for container directives
        const transformers = this.configuration.get<ContainerTransformerConstructor[]>('transformers.directive.container');
        if (transformers)
            for (const tr of transformers)
                this.container.addTransformer(tr);

    }

    public transform(node: Node, children: ChildrenData<Node, VNode>) {
        const directive = node as MDContainerDirective;

        for(const tr of this.container.getTransformers())
            if(tr.getName() === directive.name)
                return tr.transform(node, children);

        const defaultTransformerName = 'default';
        this.debug('No transformer for the container of type %o has been found.', directive.name);
        this.debug('Attempting to use %o transformer.', defaultTransformerName);

        const defaultTransformer = this.container.getTransformer(defaultTransformerName);

        if(defaultTransformer)
            return defaultTransformer.transform(node, children);
        else
            this.debug('No %o transformer found.', defaultTransformerName);

    }

}
