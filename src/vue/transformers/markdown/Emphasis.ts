import Transformer from '@hermes/common/transformer/Transformer';
import {ChildrenData} from '@hermes/common/transformer/TreeTransformer';
import {Debugger} from 'debug';
import {Node} from 'unist';
import {h, VNode} from 'vue';
import {VueConfigurationFactory} from '@hermes/vue/configuration/VueConfigurationFactory';
import VueConfiguration from '@hermes/vue/configuration/VueConfiguration';

export default class Emphasis extends Transformer<Node, VNode, VueConfiguration> {

    constructor(debug: Debugger, configuration: VueConfigurationFactory) {
        super(debug, configuration, 'emphasis');
    }

    public transform(node: Node, children: ChildrenData<Node, VNode>) {

        // Load object classes from configuration
        const classes = this.configuration.get<string[]>('theme.text.emphasis.classes');

        return h('em', {
            class: classes
        }, children.output);
    }

}
