import {Node} from 'unist';
import {Heading as MarkDownHeading} from 'mdast';
import {h, VNode} from 'vue';
import Transformer from '@hermes/common/transformer/Transformer';
import {ChildrenData} from '@hermes/common/transformer/TreeTransformer';
import {Debugger} from 'debug';
import {VueConfigurationFactory} from '@hermes/vue/configuration/VueConfigurationFactory';
import VueConfiguration from '@hermes/vue/configuration/VueConfiguration';

export default class Heading extends Transformer<Node, VNode, VueConfiguration> {

    constructor(debug: Debugger, configuration: VueConfigurationFactory) {
        super(debug, configuration, 'heading');
    }

    public transform(node: Node, children: ChildrenData<Node, VNode>) {
        const heading = node as MarkDownHeading;

        // Load object classes from configuration
        let classes = this.configuration.get<string[]>('theme.heading.classes.default');
        const classes_depth = this.configuration.get<string[]>('theme.heading.classes.depth');

        if(classes && classes_depth)
            classes = [...classes, classes_depth[`${heading.depth}`]];
        else if (classes_depth)
            classes = [classes_depth[`${heading.depth}`]];

        return h(`h${heading.depth}`, {
            class: classes
        }, children.output);
    }

}
