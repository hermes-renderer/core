import {Literal, Node} from 'unist';
import {h, VNode} from 'vue';
import Transformer from '@hermes/common/transformer/Transformer';
import {Debugger} from 'debug';
import {ChildrenData} from '@hermes/common/transformer/TreeTransformer';
import {VueConfigurationFactory} from '@hermes/vue/configuration/VueConfigurationFactory';
import VueConfiguration from '@hermes/vue/configuration/VueConfiguration';

export default class InlineCode extends Transformer<Node, VNode, VueConfiguration> {

    constructor(debug: Debugger, configuration: VueConfigurationFactory) {
        super(debug, configuration, 'inlineCode');
    }

    public transform(node: Node, children: ChildrenData<Node, VNode>) {
        const code = node as Literal;

        // Load object classes from configuration
        const classes = this.configuration.get<string[]>('theme.code.inline.classes');

        return h('code', {
            class: classes
        }, code.value as string);
    }

}
