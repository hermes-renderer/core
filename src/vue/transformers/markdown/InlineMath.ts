import {Node} from 'unist';
import {h, VNode} from 'vue';
import {InlineMath as MDInlineMath} from 'mdast-util-math';
import Transformer from '@hermes/common/transformer/Transformer';
import {Debugger} from 'debug';
import {ChildrenData} from '@hermes/common/transformer/TreeTransformer';
import katex from 'katex';
import HermesError from '@hermes/vue/components/library/HermesError.vue';
import {VueConfigurationFactory} from '@hermes/vue/configuration/VueConfigurationFactory';
import VueConfiguration from '@hermes/vue/configuration/VueConfiguration';

export default class InlineMath extends Transformer<Node, VNode, VueConfiguration> {

    constructor(debug: Debugger, configuration: VueConfigurationFactory) {
        super(debug, configuration, 'inlineMath');
    }

    public transform(node: Node, children: ChildrenData<Node, VNode>) {

        const math = node as MDInlineMath;

        // Load object classes from configuration
        const classes = this.configuration.get<string[]>('theme.math.inline.classes');

        let html: string | undefined;
        let error: unknown | undefined;

        try {
            html = katex.renderToString(math.value, {
                displayMode: false
            });
        } catch(err){
            error = err;
        }

        if(error){
            return h(HermesError, {
                class: classes,
                error: error
            });
        } else {
            return h('span', {
                class: classes,
                innerHTML: html
            });
        }

    }

}
