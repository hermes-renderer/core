import {h, VNode} from 'vue';
import {Node} from 'unist';
import {Link as MDLink} from 'mdast';
import Transformer from '@hermes/common/transformer/Transformer';
import {Debugger} from 'debug';
import {ChildrenData} from '@hermes/common/transformer/TreeTransformer';
import {VueConfigurationFactory} from '@hermes/vue/configuration/VueConfigurationFactory';
import VueConfiguration from '@hermes/vue/configuration/VueConfiguration';

export default class Link extends Transformer<Node, VNode, VueConfiguration> {

    constructor(debug: Debugger, configuration: VueConfigurationFactory) {
        super(debug, configuration, 'link');
    }

    public transform(node: Node, children: ChildrenData<Node, VNode>) {
        const link = node as MDLink;

        // Load object classes from configuration
        const classes = this.configuration.get<string[]>('theme.link.classes');

        return h('a', {
            class: classes,
            href: link.url
        }, children.output);
    }

}
