import {h, VNode} from 'vue';
import {Node} from 'unist';
import {List as MDList} from 'mdast';
import Transformer from '@hermes/common/transformer/Transformer';
import {Debugger} from 'debug';
import {ChildrenData} from '@hermes/common/transformer/TreeTransformer';
import {VueConfigurationFactory} from '@hermes/vue/configuration/VueConfigurationFactory';
import VueConfiguration from '@hermes/vue/configuration/VueConfiguration';

export default class List extends Transformer<Node, VNode, VueConfiguration> {

    constructor(debug: Debugger, configuration: VueConfigurationFactory) {
        super(debug, configuration, 'list');
    }

    public transform(node: Node, children: ChildrenData<Node, VNode>) {
        const list = node as MDList;


        if(list.ordered){
            // Load object classes from configuration
            const classes = this.configuration.get<string[]>('theme.list.ol.classes');
            return h('ol', {
                class: classes
            }, children.output);
        } else {
            // Load object classes from configuration
            const classes = this.configuration.get<string[]>('theme.list.ul.classes');
            return h('ul', {
                class: classes
            }, children.output);
        }

    }

}

