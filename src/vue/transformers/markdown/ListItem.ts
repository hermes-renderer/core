import {h, VNode} from 'vue';
import {Node} from 'unist';
import {ListItem as MDListItem} from 'mdast';
import Transformer from '@hermes/common/transformer/Transformer';
import {Debugger} from 'debug';
import {ChildrenData} from '@hermes/common/transformer/TreeTransformer';
import {VueConfigurationFactory} from '@hermes/vue/configuration/VueConfigurationFactory';
import VueConfiguration from '@hermes/vue/configuration/VueConfiguration';

export default class ListItem extends Transformer<Node, VNode, VueConfiguration> {

    constructor(debug: Debugger, configuration: VueConfigurationFactory) {
        super(debug, configuration, 'listItem');
    }

    public transform(node: Node, children: ChildrenData<Node, VNode>) {
        const item = node as MDListItem;

        // Load object classes from configuration
        const classes = this.configuration.get<string[]>('theme.list.li.classes');

        return h('li', {
            class: classes
        }, children.output);
    }

}

