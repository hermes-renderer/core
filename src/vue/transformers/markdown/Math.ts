import {Node} from 'unist';
import {h, VNode} from 'vue';
import {Math as MDMath} from 'mdast-util-math';
import Transformer from '@hermes/common/transformer/Transformer';
import {Debugger} from 'debug';
import {ChildrenData} from '@hermes/common/transformer/TreeTransformer';
import katex from 'katex';
import HermesError from '@hermes/vue/components/library/HermesError.vue';
import {VueConfigurationFactory} from '@hermes/vue/configuration/VueConfigurationFactory';
import VueConfiguration from '@hermes/vue/configuration/VueConfiguration';

export default class Math extends Transformer<Node, VNode, VueConfiguration> {

    constructor(debug: Debugger, configuration: VueConfigurationFactory) {
        super(debug, configuration, 'math');
    }

    public transform(node: Node, children: ChildrenData<Node, VNode>) {

        const math = node as MDMath;

        // Load object classes from configuration
        const classes = this.configuration.get<string[]>('theme.math.block.classes');

        let html: string | undefined;
        let error: unknown | undefined;

        try {
            html = katex.renderToString(math.value, {
                displayMode: true
            });
        } catch(err){
            error = err;
        }

        if(error){
            return h(HermesError, {
                error: error
            });
        } else {
            return h('div', {
                class: classes,
                innerHTML: html
            });
        }

    }

}
