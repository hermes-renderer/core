import {h, VNode} from 'vue';
import {Node} from 'unist';
import Transformer from '@hermes/common/transformer/Transformer';
import {Debugger} from 'debug';
import {ChildrenData} from '@hermes/common/transformer/TreeTransformer';
import {VueConfigurationFactory} from '@hermes/vue/configuration/VueConfigurationFactory';
import VueConfiguration from '@hermes/vue/configuration/VueConfiguration';

export default class Paragraph extends Transformer<Node, VNode, VueConfiguration> {

    constructor(debug: Debugger, configuration: VueConfigurationFactory) {
        super(debug, configuration, 'paragraph');
    }

    public transform(node: Node, children: ChildrenData<Node, VNode>) {

        // Load object classes from configuration
        const classes = this.configuration.get<string[]>('theme.paragraph.classes');

        return h('p', {
            class: classes
        }, children.output);
    }

}
