import Transformer from '@hermes/common/transformer/Transformer';
import {ChildrenData} from '@hermes/common/transformer/TreeTransformer';
import {Debugger} from 'debug';
import {Node} from 'unist';
import {h, VNode} from 'vue';
import {VueConfigurationFactory} from '@hermes/vue/configuration/VueConfigurationFactory';
import VueConfiguration from '@hermes/vue/configuration/VueConfiguration';

export default class Root extends Transformer<Node, VNode, VueConfiguration> {

    constructor(debug: Debugger, configuration: VueConfigurationFactory) {
        super(debug, configuration, 'root');
    }

    public transform(node: Node, children: ChildrenData<Node, VNode>) {

        // Load object classes from configuration
        const classes = this.configuration.get<string[]>('theme.container.classes');

        return h('div', {
            class: classes
        }, children.output);

    }

}
