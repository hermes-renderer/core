import Transformer from '@hermes/common/transformer/Transformer';
import {ChildrenData} from '@hermes/common/transformer/TreeTransformer';
import {Debugger} from 'debug';
import {Node} from 'unist';
import {h, VNode} from 'vue';
import {VueConfigurationFactory} from '@hermes/vue/configuration/VueConfigurationFactory';
import VueConfiguration from '@hermes/vue/configuration/VueConfiguration';

export default class Strong extends Transformer<Node, VNode, VueConfiguration> {

    constructor(debug: Debugger, configuration: VueConfigurationFactory) {
        super(debug, configuration, 'strong');
    }

    public transform(node: Node, children: ChildrenData<Node, VNode>) {

        // Load object classes from configuration
        const classes = this.configuration.get<string[]>('theme.text.strong.classes');

        return h('strong', {
            class: classes
        }, children.output);
    }

}
