import {h, Text as VueText, VNode} from 'vue';
import {ChildrenData} from '@hermes/common/transformer/TreeTransformer';
import {Debugger} from 'debug';
import {Text as MDText} from 'mdast';
import {Node} from 'unist';
import Transformer from '@hermes/common/transformer/Transformer';
import {VueConfigurationFactory} from '@hermes/vue/configuration/VueConfigurationFactory';
import VueConfiguration from '@hermes/vue/configuration/VueConfiguration';
import {render} from 'micromustache';

/**
 * This class will generate a Vue text node.
 * Optionally, it will also interpolate the contents of the text node.
 */
export default class Text extends Transformer<Node, VNode, VueConfiguration> {

    constructor(debug: Debugger, configuration: VueConfigurationFactory) {
        super(debug, configuration, 'text');
    }

    public transform(node: Node, children: ChildrenData<Node, VNode>) {

        const text_node = node as MDText;

        let text: string | undefined = text_node.value;
        let error: unknown | undefined;

        try {
            const context = this.configuration.get('context');
            if (this.configuration.get('text.interpolate') && context) {
                text = render(text, context);
            }
        } catch (err) {
            this.debug('Error rendering template: %o: %O', text, err);
        }

        return h(VueText, text);

    }

}
