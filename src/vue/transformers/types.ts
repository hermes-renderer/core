// Transformer Types
import {Debugger} from 'debug';
import Transformer from '@hermes/common/transformer/Transformer';
import {Node} from 'unist';
import {VNode} from 'vue';
import {ContainerDirective, LeafDirective, TextDirective} from 'mdast-util-directive';
import {VueConfigurationFactory} from '@hermes/vue/configuration/VueConfigurationFactory';
import VueConfiguration from '@hermes/vue/configuration/VueConfiguration';

export type MarkdownTransformerConstructor = new(debug: Debugger, options: VueConfigurationFactory) => Transformer<Node, VNode, VueConfiguration>;
export type TextTransformerConstructor = new(debug: Debugger, options: VueConfigurationFactory) => Transformer<TextDirective, VNode, VueConfiguration>;
export type LeafTransformerConstructor = new(debug: Debugger, options: VueConfigurationFactory) => Transformer<LeafDirective, VNode, VueConfiguration>;
export type ContainerTransformerConstructor = new(debug: Debugger, options: VueConfigurationFactory) => Transformer<ContainerDirective, VNode, VueConfiguration>;
