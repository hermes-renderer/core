/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        './playground/**/*.ts',
        './playground/**/*.vue',
        './src/**/*.ts'
    ],
    theme: {
        extend: {},
    },
    plugins: [],
}
