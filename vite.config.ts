import {defineConfig, UserConfigExport} from 'vite';
import * as path from 'path';
import {resolve} from 'path';
import {visualizer} from 'rollup-plugin-visualizer';
import vue from '@vitejs/plugin-vue';

const configurations: Record<string, UserConfigExport> = {

    /**
     * This configuration will generate a client bundle for the Vue renderer.
     */
    VUE_CLIENT: defineConfig(({command, ssrBuild}) => ({
        build: {
            outDir: 'dist/client/vue',
            lib: {
                entry: resolve(__dirname, 'src/vue/index.ts'),
                fileName: 'index',
                formats: ['es']
            },
            rollupOptions: {
                external: ['vue', 'katex'],
                output: {
                    globals: {
                        katex: 'katex',
                        vue: 'Vue'
                    }
                }
            },
            sourcemap: true
        },
        plugins: [
            vue(),
            visualizer({
                emitFile: true
            })
        ],
        resolve: {
            alias: {
                '@hermes': path.resolve(__dirname, './src/')
            }
        }
    })),

    /**
     * This configuration will generate a client bundle for the Vue renderer.
     */
    VUE_SERVER: defineConfig(({command, ssrBuild}) => ({
        build: {
            ssr: true,
            outDir: 'dist/server/vue',
            lib: {
                entry: resolve(__dirname, 'src/vue/index.ts'),
                fileName: 'index',
                formats: ['es']
            },
            rollupOptions: {
                external: ['vue', 'katex'],
                output: {
                    globals: {
                        katex: 'katex',
                        vue: 'Vue'
                    }
                }
            },
            sourcemap: true
        },
        plugins: [
            vue(),
            visualizer({
                emitFile: true
            })
        ],
        resolve: {
            alias: {
                '@hermes': path.resolve(__dirname, './src/')
            }
        }
    }))

};
const name: string | undefined = process.env.LIB_NAME;
const current = name ? configurations[name as string] : undefined;

if (current === undefined) {
    throw new Error('LIB_NAME is not defined or is not valid');
}

export default current;
